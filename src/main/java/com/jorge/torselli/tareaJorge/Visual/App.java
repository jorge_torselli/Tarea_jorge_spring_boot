package com.jorge.torselli.tareaJorge.Visual;

import com.jorge.torselli.tareaJorge.Dominio.Cliente;
import com.jorge.torselli.tareaJorge.Dominio.ClienteRepositorio;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by Jorge on 20/07/2017.
 */
@SpringUI
public class App extends UI {

    @Autowired
    ClienteRepositorio clienteRepositorio;

    @Override
    protected void init(VaadinRequest request) {

        VerticalLayout layout = new VerticalLayout();
        HorizontalLayout horizontalLayout = new HorizontalLayout();

        TextField nombre = new TextField("Nombre:");
        TextField direccion = new TextField("Dirección:");

        Grid<Cliente> grid = new Grid<>();
        grid.addColumn(Cliente::getCodigoCliente).setCaption("Id cliente");
        grid.addColumn(Cliente::getNombreCliente).setCaption("Nombre");
        grid.addColumn(Cliente::getDireccionCliente).setCaption("Dirección");

        Button nuevo = new Button("Nuevo cliente");
        nuevo.addClickListener(new Button.ClickListener() {
            @Override
            public void buttonClick(Button.ClickEvent event) {
                Cliente cliente = new Cliente();
                cliente.setNombreCliente(nombre.getValue());
                cliente.setDireccionCliente(direccion.getValue());
                clienteRepositorio.save(cliente);
                grid.setItems(clienteRepositorio.findAll());

                Notification.show("Cliente  correctamente agrego correctamente");

                nombre.clear();
                direccion.clear();
            }
        });

        horizontalLayout.addComponents(nombre, direccion, nuevo);
        horizontalLayout.setComponentAlignment(nuevo, Alignment.BOTTOM_RIGHT);

        layout.addComponent(horizontalLayout);
        layout.addComponent(grid);
        setContent(layout);
    }
}
