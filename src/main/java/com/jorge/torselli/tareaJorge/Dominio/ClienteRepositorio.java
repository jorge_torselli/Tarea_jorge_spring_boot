package com.jorge.torselli.tareaJorge.Dominio;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Jorge on 20/07/2017.
 */
public interface ClienteRepositorio extends JpaRepository<Cliente, Long> {
}
