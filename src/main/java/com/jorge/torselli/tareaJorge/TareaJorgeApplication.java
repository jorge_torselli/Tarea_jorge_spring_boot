package com.jorge.torselli.tareaJorge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TareaJorgeApplication {

	public static void main(String[] args) {
		SpringApplication.run(TareaJorgeApplication.class, args);
	}
}
